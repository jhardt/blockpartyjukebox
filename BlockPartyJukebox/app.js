var http = require('http');
var https = require('https');
var io = require('socket.io-client');

var jukebox = {
	lastServer: 'NONE',
	// playerUUID: '0c98df56713b4d78b53cf7461b0ce86f',
	playerUUID: null,
	socket: null,

	init: function(ign) {
		if (jukebox.playerUUID == null) {
			console.log('Retrieving player\'s UUID...');

			var url = 'https://api.mojang.com/users/profiles/minecraft/' + ign;
			https.get(url, function(response) {
				var body = '';

				response.on('data', function(chunk) {
					body += chunk;
				});

				response.on('end', function() {
					if (body != '') {
						var data = JSON.parse(body);
						if (data.id != undefined) {
							jukebox.playerUUID = data.id;
							console.log('The player\'s UUID is ' + data.id);
		
							jukebox.initSocket();
						}
					}
				});
			}).on('error', function(event) {
				console.log('An error occurred: ', event);
			});
		} else {
			jukebox.initSocket();
		}
	},

	initSocket: function() {
		console.log('Connecting to HiveMC BlockParty socket...');
		jukebox.socket = io.connect('http://91.121.116.23:9005');
		jukebox.socket.on('connect', jukebox.eventConnect);
		jukebox.socket.on('control', jukebox.eventControl);
		jukebox.socket.on('endgame', jukebox.eventEndgame);
	
		jukebox.trackUser();
	},

	joinServer: function(server) {
		if (server != 'NONE') {
			jukebox.socket.emit('joinserver', {server: server});
		}
	},

	leaveServer: function(server) {
		if (server != 'NONE') {
			jukebox.socket.emit('leaveserver', {server: server});
		}
	},

	trackUser: function() {
		console.log('Tracking the user\'s status...');
		var url = 'http://hivemc.com/json/getblockpartyserver/' + jukebox.playerUUID;
		http.get(url, function(response) {
			var body = '';

			response.on('data', function(chunk) {
				body += chunk;
			});

			response.on('end', function() {
				if (body != '') {
					var data = JSON.parse(body);
					console.log('User is connected to: ' + data.server);

					if (data.server != undefined && data.server != jukebox.lastServer) {
						jukebox.leaveServer(jukebox.lastServer);
						jukebox.joinServer(data.server)
						jukebox.lastServer = data.server;
					}
				}

				setTimeout(jukebox.trackUser, 20000);
			});
		}).on('error', function(event) {
			console.log('An error occurred: ', event);
		});
	},

	sendPlay: function() {
		console.log('[S] Starting music on request.');
	},

	sendPause: function() {
		console.log('[S] Pausing music on request.');
	},

	eventConnect: function(event) {
		console.log('Connected to HiveMC BlockParty socket.');
	},

	eventControl: function(event) {
		if (event.data.play == 1) {
			jukebox.sendPlay();
		} else {
			jukebox.sendPause();
		}
	},

	eventEndgame: function(event) {
		jukebox.sendPause();
		console.log('Round ended.');
	}
};

process.stdin.setEncoding('utf8');
process.stdin.on('readable', function() {
  var chunk = process.stdin.read();
  if (chunk !== null) {
    process.exit(0);
  }
});

if (process.argv[2] == undefined) {
	console.log('No username specified.');
}
else {
	jukebox.init(process.argv[2]);
}

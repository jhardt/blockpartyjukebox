﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlockPartyJukebox
{
    public partial class BlockPartyForm : Form
    {
        private Process listenerProcess;

        public BlockPartyForm()
        {
            InitializeComponent();
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(playerTextBox.Text))
            {
                MessageBox.Show("Please enter a valid Minecraft username!");
                return;
            }

            connectButton.Enabled = false;
            playerTextBox.Enabled = false;
            statusLabel.Text = "Connecting...";
            Thread workerThread = new Thread(new ThreadStart(startListener));
            workerThread.Start();
        }

        private void startListener()
        {
            // ProcessStartInfo startInfo = new ProcessStartInfo("cmd", "/c " + "node app.js " + playerTextBox.Text)

            ProcessStartInfo startInfo = new ProcessStartInfo("node", "app.js " + playerTextBox.Text)
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardInput = true,
                CreateNoWindow = true,
                WorkingDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "BlockPartyJukebox")
            };

            listenerProcess = Process.Start(startInfo);
            listenerProcess.OutputDataReceived += (sender, e) => Debug.WriteLine(e.Data);
            listenerProcess.OutputDataReceived += listener_OutputDataReceived;

            listenerProcess.BeginOutputReadLine();
            listenerProcess.Start();
            listenerProcess.WaitForExit();
            // We may not have received all the events yet!
            Thread.Sleep(5000);
        }

        private void listener_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data.Contains("[S]"))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "BlockPartyJukebox", "spotify_cmd.exe"), "playpause")
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    UseShellExecute = false,
                    RedirectStandardOutput = false,
                    CreateNoWindow = true,
                };

                Process process = Process.Start(startInfo);
                return;
            }

            if (InvokeRequired)
            {
                Invoke((MethodInvoker)delegate { listener_OutputDataReceived(sender, e); });
                return;
            }

            if (e.Data.Contains("Connected"))
            {
                statusLabel.Text = "Have fun!";
                return;
            }

            if (e.Data.Contains("An error occurred"))
            {
                MessageBox.Show("An error occurred or you entered an incorrect username. Please try again.");
                connectButton.Enabled = true;
                playerTextBox.Enabled = true;
                statusLabel.Text = "Please enter your IGN.";
            }
        }

        private void BlockPartyForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (listenerProcess != null && !listenerProcess.HasExited)
            {
                listenerProcess.StandardInput.WriteLine("close");
                listenerProcess.StandardInput.Flush();
            }
        }
    }
}
